package org.vaadin.example;

import com.vaadin.cdi.annotation.CdiComponent;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;

import com.vaadin.flow.shared.Registration;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Consumer;

/**
 * The main view contains a text field for getting the user name and a button
 * that shows a greeting message in a notification.
 */
@Route("")
@CdiComponent
@Slf4j
public class MainView extends VerticalLayout implements Consumer<String> {

    @Inject
    private Broadcaster broadcaster;
    private Registration broadcasterRegistration;

    @PostConstruct
    public void init() {
        TextField textField = new TextField("Your message");

        Button button = new Button("Broadcast", e -> broadcaster.broadcast(textField.getValue()));
        button.addClickShortcut(Key.ENTER);

        add(textField, button);
        broadcasterRegistration = broadcaster.register(this);
    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        super.onDetach(detachEvent);
        if (broadcasterRegistration != null) {
            broadcasterRegistration.remove();
            broadcasterRegistration = null;
        }
        log.info(this + " was detached");
    }

    @Override
    public void accept(String message) {
        getUI().ifPresent(ui -> ui.access(() -> Notification.show(message)));
    }
}
