package org.vaadin.example;

import com.vaadin.flow.shared.Registration;
import jakarta.annotation.Resource;
import jakarta.enterprise.concurrent.ManagedExecutorService;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;

@ApplicationScoped
@Slf4j
public class Broadcaster {
    private final ConcurrentLinkedQueue<Consumer<String>> listeners = new ConcurrentLinkedQueue<>();
    @Resource
    private ManagedExecutorService executor;

    public Registration register(Consumer<String> listener) {
        log.info("Registering " + listener);
        listeners.add(listener);
        return () -> {
            log.info("Unregistering " + listener);
            listeners.remove(listener);
        };
    }

    public void broadcast(String message) {
        log.info("Broadcasting '" + message + "' to " + listeners);
        for (Consumer<String> listener : listeners) {
            executor.execute(() -> listener.accept(message));
        }
    }
}